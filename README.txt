
-- SUMMARY --

Menu Valid Target allows users of your site to open menu links in a new tab or
window. This is done by only using Javascript without the deprecated target
attribute in order to be XHTML compliant. Menus can be configured to allow for
this setting on their menu links.

Links in such menus provide an option to open them in a new window. This is also
available when editing a node under the "Menu settings" tab.

Disclosure: This is a clone of Menu Target module
(https://drupal.org/project/menu_target). The only differences are:

1. No option to use the deprecated target attribute.
2. Menus are configurable to allow menu valid target setting when editing links.


-- REQUIREMENTS --

* None.


-- INSTALLATION --

* Install as usual, see
  http://drupal.org/documentation/install/modules-themes/modules-7

-- CONFIGURATION --

* Go to admin/structure/menu/settings and edit the message to indicate what will
happen when a user clicks on a link.

* Go to admin/structure/menu and click on the "Edit menu" link for the menu you
would like to enable menu valid target settings for and tick the checkbox
"Enable menu valid target for this menu."


-- CONTACT --

Current maintainers:
* Deji Akala (dakala) - https://drupal.org/user/53175

This project has been sponsored by:
* The British Council
